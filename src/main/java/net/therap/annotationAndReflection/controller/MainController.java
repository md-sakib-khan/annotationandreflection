package net.therap.annotationAndReflection.controller;

/**
 * @author sakib.khan
 * @since 12/18/21
 */
public class MainController {

    public static void main(String[] args) {

        try {
            new PersonValidationController().init();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}