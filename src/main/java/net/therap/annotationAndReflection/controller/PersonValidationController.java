package net.therap.annotationAndReflection.controller;

import net.therap.annotationAndReflection.model.Person;
import net.therap.annotationAndReflection.model.ValidationError;
import net.therap.annotationAndReflection.service.AnnotatedValidationService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sakib.khan
 * @since 12/18/21
 */
public class PersonValidationController {

    private AnnotatedValidationService annotatedValidator;

    PersonValidationController(){
        annotatedValidator = new AnnotatedValidationService();
    }

    public void init() throws IllegalAccessException {
        Person person = new Person("Md. Rakib", 25);
        List<ValidationError> errors = new ArrayList<>();
        annotatedValidator = new AnnotatedValidationService();
        annotatedValidator.validate(person, errors);
        annotatedValidator.print(errors);
    }
}