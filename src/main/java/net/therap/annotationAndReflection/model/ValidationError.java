package net.therap.annotationAndReflection.model;

/**
 * @author sakib.khan
 * @since 12/18/21
 */
public class ValidationError {

    private String fieldName;
    private String message;

    public ValidationError(String name, String message) {
        this.fieldName = name;
        this.message = message;
    }

    public String getErrorFieldName() {
        return fieldName;
    }

    public String getErrorMessage() {
        return message;
    }
}