package net.therap.annotationAndReflection.model;

import java.lang.annotation.*;

/**
 * @author sakib.khan
 * @since 12/18/21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Size {

    int max() default 100;

    int min() default 1;

    String message() default "Length must be {min}-{max}";
}