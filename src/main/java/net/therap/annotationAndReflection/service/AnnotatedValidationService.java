package net.therap.annotationAndReflection.service;

import net.therap.annotationAndReflection.model.Size;
import net.therap.annotationAndReflection.model.ValidationError;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;

/**
 * @author sakib.khan
 * @since 12/18/21
 */
public class AnnotatedValidationService {

    public <T> void validate(T obj, List<ValidationError> errors) throws IllegalAccessException {

        Field[] attributes = obj.getClass().getDeclaredFields();

        for (Field attribute : attributes) {

            if (!attribute.isAccessible()) {
                attribute.setAccessible(true);
            }

            Annotation[] annotations = attribute.getAnnotations();

            for (Annotation annotation : annotations) {

                if(annotation instanceof Size){

                    Size size = (Size) annotation;
                    int max = size.max();
                    int min = size.min();
                    String message = size.message();

                    String attributeName = attribute.getName();
                    String attributeType = String.valueOf(attribute.getType().getTypeName());
                    attributeType = attributeType.replaceAll("java.lang.","");
                    int attributeSize = getAttributeSize(attribute, obj);

                    checkSizeValidation(attributeName, attributeSize, attributeType, min, max, message, errors);
                }
            }
        }
    }

    public void print(List<ValidationError> errors) {
        if (errors.size() == 0) {
            System.out.println("No Error");
            return;
        }
        for (ValidationError validationError : errors) {
            System.out.println(validationError.getErrorFieldName() + " : " + validationError.getErrorMessage());
        }
    }

    private void checkSizeValidation(String attributeName, int attributeSize, String attributeType, int min, int max,
                                     String message, List<ValidationError> errors) {
        if (attributeSize < min || attributeSize > max) {
            message = message.replaceAll("\\{min}", String.valueOf(min));
            message = message.replaceAll("\\{max}", String.valueOf(max));
            errors.add(new ValidationError(attributeName + "(" + attributeType + ")", message));
        }
    }

    private <T> int getAttributeSize(Field attribute, T obj) throws IllegalAccessException {
        Object candidateForValidation = attribute.get(obj);
        if (candidateForValidation instanceof String) {
            return ((String) candidateForValidation).length();
        } else if (candidateForValidation instanceof Integer) {
            return (int) candidateForValidation;
        }
        return 0;
    }
}